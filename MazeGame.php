<?php
require_once __DIR__ . '\vendor\autoload.php';
use Symfony\Component\Console\Application;
use Console\Initialisation;

//very initial iteration of code
$app = new Application('Maze Game', 'v1.0.0');
//Add in initial starting command
$app->add(new Initialisation());
$app->run();