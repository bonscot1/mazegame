<?php namespace Console;

use Console\General;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;


class Game extends General
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Setup the map and prepare globals
     *
     * @param Array $maze
     * @param Array $inRoom
     * @param String $user
     * @param $output
     * @param $input
     * @param $questionHelper
     * @return void
     */
    public function game($maze, $inRoom, $user, $output, $input, $questionHelper)
    {
        $this->enemy = [];
        $this->inRoom = $inRoom;
        $this->maze = $maze;
        $this->finish = $this->item = false;
        $this->goldCount = 0;
        $this->room = $this->findRoom($this->inRoom);
        $this->randomIntro($user, $output);
        while (true != $this->finish) {
            $this->roomChange($user, $output, $input, $questionHelper);
        }
        $this->endGame($user, $output, $input, $questionHelper);
    }

    /**
     * Find the initial spawn room
     *
     * @return void
     */
    private function findRoom()
    {
        return $this->maze[$this->inRoom['y']][$this->inRoom['x']];
    }

    /**
     * Assigns random flavour text intro to the story. This is displayed and forgotten about
     *
     * @param String $user
     * @param $output
     * @return void
     */
    private function randomIntro($user, $output)
    {
        //grab all of the intros and pick a random one for the user
        $intros = array_diff(scandir('src/intros'), ['.', '..']);
        foreach($intros as $intro) {
            $formattedIntros[] = $intro;
        }
        $index = rand(0, sizeof($formattedIntros)-1);
        $file = $formattedIntros[$index];
        $intro = file('src/intros/'.$file, FILE_IGNORE_NEW_LINES);
        $this->outputText($intro, $output);
    }

    /**
     * When the user changes room
     *
     * @param String $user
     * @param $output
     * @param $input
     * @param $questionHelper
     * @return void
     */
    private function roomChange($user, $output, $input, $questionHelper)
    {
        $choice = $this->giveRoomDescription($user, $output, $input, $questionHelper);
        $this->doAction($choice, $output);
    }

    /**
     * A description of the room and actions for the user on loading the room
     *
     * @param String $user
     * @param $output
     * @param $input
     * @param $questionHelper
     * @return void
     */
    private function giveRoomDescription($user, $output, $input, $questionHelper)
    {
        $exits = 0;
        $availableExits = '';
        $outputText = [];
        foreach ($this->room as &$attribute) {
            switch ($attribute) {
                case 'north':
                case 'east':
                case 'south':
                case 'west':
                    $exits += 1;
                    $availableExits = $availableExits . $attribute .', ';
                    break;
                case 'enemy':
                    if ([]==$this->enemy) {
                        $this->enemy = $this->randomEnemy();
                    };
                    $outputText[] = '<error>There is a rather '. $this->enemy['emotion'] .' looking ' . $this->enemy['name'] . ' glaring at you. ' . $this->enemy['desc'].'</error>';
                    $outputText[] = $this->enemy['action'];
                    break;
                case 'nodescription':
                    $outputText[] = $this->randomRoomDescription();
                    break;
                case 'empty':
                    $outputText[] = 'You see a space where some gold used to be...';
                    break;
                case 'item':
                    $outputText[] = $this->grabItem();
                    $attribute = 'empty';
                    break;
                case 'exit':
                    $outputText[] = 'You spot a ladder and see daylight spilling in from the corner of the room';
                    break;
            }
            if (is_numeric($attribute)) {
                $this->goldCount += $attribute;
                $outputText[] = '<comment>You picked up '.$attribute.' gold!</comment>';
                $attribute = 'empty';
            }
        }
        $this->maze[$this->inRoom['y']][$this->inRoom['x']] = $this->room;
        //Flavour text and information for the user to use to navigate
        $outputText[] = 'There are '.$exits.' exits to this room, '.$availableExits. 'pick wisely.';
        $this->outputText($outputText, $output);
        return $this->getQuestion('What would you like to do, '.$user.'? ', 'nothing', $questionHelper, $input, $output);
    }

    /**
     * Assign a random enemy to a room that an enemy is in
     *
     * @return Array
     */
    private function randomEnemy()
    {
        //fetch all enemies from the enemy class (This is just currently an array of arrays)
        $enemies = Enemy::getEnemies();
        return $enemies[rand(0, sizeof($enemies)-1)];
    }

    /**
     * Assign a room description if asked for
     *
     * @return void
     */
    private function randomRoomDescription()
    {
        //Placeholder for if I add nodescription to a room
        return 'The stone walls are damp, a golden chandelier hangs from the ceiling.';
    }

    /**
     * When the user types a command
     *
     * @param String $unformattedChoice
     * @param $output
     * @return void
     */
    private function doAction($unformattedChoice, $output)
    {
        //Take out the prefix to see if a move command
        $choice = explode(" ", $unformattedChoice);
        $prefix = $choice[0];
        //All possible commands
        switch ($prefix) {
            case 'move':
            case 'walk':
            case 'go':
                $message = $this->movement($choice, $output);
                break;
            case 'attack':
            case 'hit':
                $message = $this->attack();
                break;
            case 'exit':
            case 'leave':
                $message = $this->exit();
                break;
            case 'gold':
                $message = '<comment>I have managed to get, '.$this->goldCount.' gold so far.</comment>';
                break;
            case 'help':
                $message = 'No one can help you here!';
                $help = [
                    '',
                    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
                    'Use move/go/walk {direction} to move in a direction',
                    'Use attack/hit to kill the enemy in the room',
                    'Use exit/leave once you reach the exit in order to leave the maze',
                    'Use gold to check your current gold status',
                    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
                    ''
                ];
                $this->outputText($help, $output);
                break;
            default:
                $message = 'You start to dribble as you get more confused by the commands your brain is giving you';
                break;

        }
        $this->outputText(['', $message, ''], $output);
    }

    /**
     * When the user attempts to move in a direction
     *
     * @param Array $movement
     * @param $output
     * @return String
     */
    private function movement($movement, $output)
    {
        $direction = $movement[1];
        switch ($direction) {
            case 'north':
            //Make sure there is an option to go that direction
                if (in_array('north', $this->room)) {
                    if (in_array('enemy', $this->room)) {
                        return $this->movementEnemyPresent($output);
                    } else {
                        $this->inRoom['y'] -=1;
                        $this->room = $this->maze[$this->inRoom['y']][$this->inRoom['x']];
                        return 'You move north';
                    }
                }
                return 'You run into the northern wall';
                break;
            case 'south':
            //Make sure there is an option to go that direction
                if (in_array('south', $this->room)) {
                    if (in_array('enemy', $this->room)) {
                        return $this->movementEnemyPresent($output);
                    } else {
                        $this->inRoom['y'] +=1;
                        $this->room = $this->maze[$this->inRoom['y']][$this->inRoom['x']];
                        return 'You move south';
                    }
                }
                return 'You run into the southern wall';
                break;
            case 'west':            
            //Make sure there is an option to go that direction
                if (in_array('west', $this->room)) {
                    if (in_array('enemy', $this->room)) {
                        return $this->movementEnemyPresent($output);
                    } else {
                        $this->inRoom['x'] -=1;
                        $this->room = $this->maze[$this->inRoom['y']][$this->inRoom['x']];
                        return 'You move west';
                    }
                }
                return 'You run into the western wall';
                break;
            case 'east':
            //Make sure there is an option to go that direction
                if (in_array('east', $this->room)) {
                    if (in_array('enemy', $this->room)) {
                        return $this->movementEnemyPresent($output);
                    } else {
                        $this->inRoom['x'] +=1;
                        $this->room = $this->maze[$this->inRoom['y']][$this->inRoom['x']];
                        return 'You move east';
                    }
                }
                return 'You run into the eastern wall';
                break;
            default:
            //If move, but not a valid direction
                return 'That is not a recognised direction';
        }
    }

    /**
     * If the user attempts to move when in a room with an enemy
     *
     * @param $output
     * @return String
     */
    private function movementEnemyPresent($output)
    {
        $goldLoss = rand(0, ($this->goldCount/2));
        $this->goldCount -= $goldLoss;
        $this->outputText(['', 'You lost ' . $goldLoss . ' gold from trying to run away! ', 'You now only have ' . $this->goldCount .' gold!'], $output);
        return $this->enemy['escape'];
    }

    /**
     * When the user attacks an enemy
     *
     * @return String
     */
    private function attack()
    {
        //Make sure there is an enemy in the room
        if (in_array('enemy', $this->room)) {
            if (false == $this->item) {
                if(1 === rand(0,2)) {
                    $message = 'You hit the ' . $this->enemy['name']. '. It is still standing!';
                } else {
                    $this->room = array_diff($this->room, ['enemy']);
                    $this->maze[$this->inRoom['y']][$this->inRoom['x']] = $this->room;
                    $message = 'You hit the ' . $this->enemy['name']. '. It dies.';
                    $this->enemy = [];
                }
            } else {
                $this->room = array_diff($this->room, ['enemy']);
                $this->maze[$this->inRoom['y']][$this->inRoom['x']] = $this->room;
                $message = 'You '.$this->item['attack'].' the '.$this->enemy['name'].' with your '.$this->item['name'].'. It dies.';
                $this->enemy = [];
            }
            return $message;
        }
        return 'You punch the floor. Your hand now hurts.';
    }

    /**
     * When the user enters a room with an item
     *
     * @return String
     */
    private function grabItem()
    {
        //fetch all items from the item class (This is just currently an array of arrays)
        $items = Item::getItems();
        $this->item = $items[rand(0, sizeof($items)-1)];
        return 'You grab the '.$this->item['name']. '. That could deal a lot of damage!';
    }

    /**
     * When the user attempts to exit the maze
     *
     * @return void
     */
    private function exit()
    {
        //Make sure there is an exit in the room
        if (in_array('exit', $this->room)) {
            $this->finish = true;
            return 'You climb down a ladder in the corner of the room...';
        }
        return 'There is no escape!';
    }

    /**
     * When the user escapes successfully, they get added to and shown a leaderboard
     *
     * @param String $user
     * @param $output
     * @param $input
     * @param $questionHelper
     * @return void
     */
    private function endGame($user, $output, $input, $questionHelper)
    {
        //Outro text
        $outputText = [
            '',
            'Congratulations, you escaped the maze with '.$this->goldCount.' gold!',
            '',
            '#################################LEADERBOARD#################################',
        ];

        //Implement a basic leaderboard system to give gold collection meaning
        $leaderboard = unserialize(base64_decode(file_get_contents('src/extras/leaderboard.bin')));
        $placed = false;
        //If first entry make a new leaderboard, if new entry compare against other scores
        if (null != $leaderboard) {
            foreach ($leaderboard as $player) {
                $split = explode(' - ', $player);
                $money = (int)$split[1];
                if(true === $placed) {
                    $updatedLeaderboard[] = $player;
                } else if ($money<$this->goldCount) {
                    $updatedLeaderboard[] = $user . ' - '.$this->goldCount;
                    $updatedLeaderboard[] = $player;
                    $placed = true;
                } else {
                    $updatedLeaderboard[] = $player;
                }
            }
            if (false == $placed) {
                $updatedLeaderboard[] = $user . ' - '.$this->goldCount;;
            }
        } else {
            $updatedLeaderboard[] = $user . ' - '.$this->goldCount;
        }

        //Merge the leaderboard text and the scores
        $outputText = array_merge($outputText, $updatedLeaderboard);

        //put the leaderboard back and encode + serialise the array
        file_put_contents('src/extras/leaderboard.bin', base64_encode(serialize($updatedLeaderboard)));

        //Output the leaderboard stuff
        $this->outputText($outputText, $this->output);
        $this->getQuestion('Please press enter to return to the menu', 'ok', $questionHelper, $input, $output);
    }
}