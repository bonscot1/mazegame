<?php namespace Console;

use Console\Game;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;


class MainMenu extends Game
{

    public function __construct()
    {
        parent::__construct();
    }

    protected function startGame(InputInterface $input, OutputInterface $output)
    {
        //Globally set the input, output, question helper, and user variable
        $this->input = $input;
        $this->output = $output;
        $this->questionHelper = $this->getHelper('question');
        $this->user = $this->input->getArgument('username');
        //Initial greeting message for the user
        $outputText = [
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| Welcome to the maze         |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| Dont expect to escape alive |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|1 - Start the game           |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|2 - Change your name         |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|3 - Run in fear              |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
        ];
        $this->outputText($outputText, $this->output);
        //Get the menu option question and ask the question
        $this->menuOptions();
        //return to the menu
        $this->startGame($input, $output);
    }

    /**
     * Answering menu choices
     *
     * @return void
     */
    private function menuOptions()
    {
        $wording = 'What would you like to do, '.$this->user.'? ';
        $menuChoice = $this->getQuestion($wording, 'unknown', $this->questionHelper, $this->input, $this->output);

        //Check the user's reply/selection and cause this to lead to one of the possible options
        switch($menuChoice) {
            case 1; //supply a few options as user's commonly type the wrong thing on first attempt
            case 'start';
            case 'play';
                $this->chooseMaze();
                break;
            
            case 2;
            case 'rename';
            case 'name';
                $this->chooseName();
                break;
            
            case 3;
            case 'run';
            case 'exit';
                $this->goodbye();
                break;
            
            default: //Any options that weren't expecting
                $this->unknown();
                break;
        }
    }

    /**
     * Menu option in which leads to picking the maze file
     *
     * @return void
     */
    private function chooseMaze()
    {
        $outputText = [
            'Initiating maze picker...',
        ];
        $this->outputText($outputText, $this->output);

        $this->mazePicker();
    }

    /**
     * Allows the user to rename their temporary character profile
     *
     * @return void
     */
    private function chooseName()
    {   
        $wording = 'Is '.$this->user.' not good enough for you? What would you prefer? ';
        $this->user = $this->getQuestion($wording, $this->user, $this->questionHelper, $this->input, $this->output);
        //Return to the menu
        $this->menuOptions();
    }

    /**
     * Exit the app
     *
     * @return void
     */
    private function goodbye()
    {
        $outputText = [
            "You can run, but you can't hide",
        ];
        $this->outputText($outputText, $this->output);
        die;
    }

    /**
     * Any unknown menu responses
     *
     * @return void
     */
    private function unknown()
    {
        $outputText = [
            'What are you even saying? This reply is invalid, try again... '
        ]; //Output relevant text that informs the user that their choice didn't make sense to the app
        $this->outputText($outputText, $this->output);
        $this->menuOptions();
    }

    /**
     * Start the maze picker process
     *
     * @return void
     */
    private function mazePicker()
    {
        $outputText = [
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            'Maze picker succesfully initialised!',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            'Type "back" to return to the main menu',
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
            'Your possible choices are below: ',
            ''
        ];
        //Fetch all maze file names - they are the names of the mazes
        //Remove the parent directory representation and the example map text file
        $mazes = array_diff(scandir('src/mazes'), ['.', '..', 'XExample.txt']);
        //Put the files in a more displayable format. The number will represent the input the user will have to give
        foreach($mazes as $key => $maze) {
            $mazeOutput[] = $key-1 . ' - ' . str_replace(".txt", "", $maze);
        }
        //Add the maze names onto the end of the text to display to the user - this way they know their options
        $outputText = array_merge($outputText, $mazeOutput,['']);

        $this->outputText($outputText, $this->output);
        $question = 'Pick your chosen maze: ';
        $mazeChoice = $this->getQuestion($question, 'unknown', $this->questionHelper, $this->input, $this->output);

        //Send to a function which validates and formats the maze appropriately
        $generated = $this->generateMaze($mazeChoice, $mazes);
        
        $mazeMap = $generated[0];
        $inRoom = $generated[1];

        // No issues with any of the attributes or rooms was found
        $this->outputText(['', 'No issues found in the maze data!', "Let's begin, " . $this->user, '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~', ''], $this->output);

        $this->game($mazeMap, $inRoom, $this->user, $this->output, $this->input, $this->questionHelper);
    }

    /**
     * Takes the chosen maze and forms a useful array
     *
     * @param String $mazeChoice
     * @param Array $mazes
     * @return Array
     */
    private function generateMaze($mazeChoice, $mazes)
    {
        //Validating the map chosen is an actual choice
        if ('unknown'===$mazeChoice) {
            $this->outputText(['There is an issue with your input, try again.'], $this->output);
        } else if ('back'===$mazeChoice) {
            $this->menuOptions();
            die;
        }

        $file = $mazes[$mazeChoice+1];
        $this->outputText(['','Loading '.$file.' please wait patiently'], $this->output);
        
        //Grab the file and tern it into an array with each line as a string
        $rawMazeMap = array_diff(file('src/mazes/'.$file, FILE_SKIP_EMPTY_LINES|FILE_IGNORE_NEW_LINES), ['[',']','{','}']);
        $mazeMap = [];

        //X is row Y is column Z is a attribute of the room
        $x = $y = $z = 0;
        //format the map as a multidimentional array in order to replicate a top down map
        foreach ($rawMazeMap as $row) {
            //In the text file, a stop represents a new column
            if ('stop'==$row) {
                $x += 1;
                $z = 0;
                continue;
                //In the text file row represents a new row
            } else if ('row'==$row) {
                $y += 1;
                $x = $z = 0;
                continue;
            }
            //Seperate out all the attributes into their associated rooms
            $mazeMap[$y][$x][$z] = $row;
            //Next attribute
            $z += 1;
        }
        //If the first part takes a long time due to it being a long file, this gives the user a sign of progress
        $this->outputText(['','Maze imported, processing and validating maze data... '], $this->output);

        //Maze type 1 means that the user will have a random start
        $mazeType = 1;
        $errors = [];

        //Go through EVERY attribute of EVERY room of EVERY column
        foreach ($mazeMap as $row => &$rowData) {
            foreach ($rowData as $column => &$columnData) {
                foreach ($columnData as &$attribute) {
                    switch ($attribute) {
                        case 'north':
                        //Make sure that travel is enabled both ways
                            if (!in_array('south', $mazeMap[$row-1][$column])) {
                                $errors[] = 'Room '.$row.', '.$column.' cannot connect north';
                            }
                            break;
                        case 'south':
                        //Make sure that travel is enabled both ways
                            if (!in_array('north', $mazeMap[$row+1][$column])) {
                                $errors[] = 'Room '.$row.', '.$column.' cannot connect south';
                            }
                            break;
                        case 'west':
                        //Make sure that travel is enabled both ways
                            if (!in_array('east', $mazeMap[$row][$column-1])) {
                                $errors[] = 'Room '.$row.', '.$column.' cannot connect west';
                            }
                            break;
                        case 'east':
                            //Make sure that travel is enabled both ways
                            if (!in_array('west', $mazeMap[$row][$column+1])) {
                                $errors[] = 'Room '.$row.', '.$column.' cannot connect east';
                            }
                            break;
                        case 'gold':
                            //Give a room a random amount of gold
                            $attribute = rand(1,100);
                            break;
                        case 'enemy':
                            break;
                        case 'item':
                            //Add in way to have a random weapon in the room
                            break;
                        case 'nodescription':
                            //Add in random description for the room
                            //$attribute = Description::random();
                            break;
                        case 'start':
                            //If a start room has already been found previously, it is an error, otherwise the maze is maze type 0
                            //Maze type 0 has a default maze spawn location
                            //Maze type 1 has no default spawn location and is assigned a random spawn
                            if (1!==$mazeType) {
                                $errors[] = 'You have too many starting rooms!';
                            }
                            $inRoom = [
                                'x' => $column,
                                'y' => $row
                            ];
                            $mazeType = 0;
                        case 'exit':
                            //No need to do anything on an exit
                            break;
                        default:
                            //Anything not specified or already removed is data that shouldn't be in the file
                            $errors[] = 'Room '.$row.', '.$column.' contains invalid data, ' . $attribute;
                    }
                }
            }
        }

        if (1 == $mazeType) {
            $output= $this->setStart($mazeMap);
            $mazeMap = $output[0];
            $inRoom = $output[1];
        }

        if ([] !== $errors) {
            $this->outputText(array_merge(['','Errors with maze: '], $errors), $this->output);
            die;
        }
        return array($mazeMap, $inRoom);
    }

    /**
     * Give a random room the start attribute
     *
     * @param Array $mazeMap
     * @return Array
     */
    private function setStart($mazeMap)
    {
        //Fetch a random row
        $y = rand(0,sizeof($mazeMap)-1);
        //Fetch a random room in that row
        $x = rand(0, sizeof($mazeMap[$y])-1);
        //Add start onto the end of the rooms array
        if (sizeof($mazeMap[$y][$x]) < 1) {
            //recursively use this function until a good random room is found that can be started in
            $mazeMap = $this->setStart($mazeMap);
        } else {
            $mazeMap[$y][$x][] = 'start';
            $inRoom = [
                'x' => $x,
                'y' => $y
            ];
            $mazeMap = array($mazeMap, $inRoom);
        }
        return $mazeMap;
    }
}