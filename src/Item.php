<?php namespace Console;

use Console\General;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;


class Item extends General
{

    public function __construct()
    {
        parent::__construct();
    }

    public static function getItems()
    {
        return [
            [
                'name'      => 'sword',
                'attack'    => 'slash'
            ],
            [
                'name'      => 'club',
                'attack'    => 'blugeon'
            ],
            [
                'name'      => 'whip',
                'attack'    => 'slash'
            ],
            [
                'name'      => 'iron glove',
                'attack'    => 'punch'
            ],
            [
                'name'      => 'hammer',
                'attack'    => 'smash'
            ]
        ];
    }
}