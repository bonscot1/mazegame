<?php namespace Console;

use Console\General;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;


class Enemy extends General
{

    public function __construct()
    {
        parent::__construct();
    }

    public static function getEnemies()
    {
        return [
            [
                'name'      => 'ogre',
                'desc'      => "It's eyes glazed over with rage.",
                'action'    => '"You no leave this room!" it roars',
                'emotion'   => 'angry',
                'escape'    => '"No no no, you no get past me"'
            ],
            [
                'name'      => 'goblin',
                'desc'      => "It's little arms reaching for it's sword.",
                'action'    => '"You got some gold you you?" it snarls',
                'emotion'   => 'devious',
                'escape'    => '"You arent going anywhere with that gold!"'
            ],
            [
                'name'      => 'spectre',
                'desc'      => "You start to see your breath.",
                'action'    => '*Screeching*',
                'emotion'   => 'empty',
                'escape'    => '"There is no escape!"'
            ],
            [
                'name'      => 'witch',
                'desc'      => "It's face wrinkled with age.",
                'action'    => '"You cannot leave this place" she cackles',
                'emotion'   => 'old',
                'escape'    => '"Stay still before I turn you into a frog!" she cackles'
            ],
            [
                'name'      => 'zombie',
                'desc'      => "It's body writhing with maggots.",
                'action'    => '"uggggggggh. my braaAAins" it groans',
                'emotion'   => 'decrepit',
                'escape'    => '"BRAAAAAAAAAAINS" it screams, clawing in your direction'
            ],
            [
                'name'      => 'gelatinous cube',
                'desc'      => "It wobbles.",
                'action'    => '*wobble*',
                'emotion'   => 'wobbly',
                'escape'    => '*wobbling aggressively*'
            ]
        ];
    }
}