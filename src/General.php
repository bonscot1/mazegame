<?php namespace Console;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;


class General extends SymfonyCommand
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a new question for the user to answer
     *
     * @param String $question
     * @param $default
     * @return Question
     */
    protected function getQuestion(String $question, $default, $questionHelper, InputInterface $input, OutputInterface $output)
    {
        $question = new Question($question, $default);
        return $questionHelper->ask($input, $output, $question);
    }

    /**
     * An array of text is outputted to the console
     *
     * @param Array $text
     * @return void
     */
    protected function outputText(Array $text, OutputInterface $output)
    {
        $output->writeln($text);
    }
}