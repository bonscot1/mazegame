<?php namespace Console;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Console\MainMenu;



class Initialisation extends MainMenu
{
    
    public function configure()
    {
        $this->setName('start')
            ->setDescription('Start up the maze game, include your name.')
            ->setHelp('This command launches the maze game! You must include your character name when you run the command')
            ->addArgument('username', InputArgument::REQUIRED, 'Your characters name');
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->startGame($input, $output);
    }
}